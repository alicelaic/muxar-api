package muxar.gateway.dto;

/**
 * Created by Alice on 1/29/2016.
 */
public class TokenWrapper {

    private String userId;
    private String token;

    public TokenWrapper(String userId, String token) {
        this.userId = userId;
        this.token = token;
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public String getToken() {
        return token;
    }

    public void setToken(String token) {
        this.token = token;
    }
}
