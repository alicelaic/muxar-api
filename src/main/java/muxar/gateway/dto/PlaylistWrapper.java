package muxar.gateway.dto;

import muxar.playlist.domain.Playlist;

import java.util.List;

/**
 * Class used to wrap over list using rest template
 * Created by Alice on 1/28/2016.
 */
public class PlaylistWrapper {
    private List<Playlist> playlistList;

    public List<Playlist> getPlaylistList() {
        return playlistList;
    }

    public void setPlaylistList(List<Playlist> playlistList) {
        this.playlistList = playlistList;
    }
}
