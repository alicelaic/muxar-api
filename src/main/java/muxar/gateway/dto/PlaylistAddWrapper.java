package muxar.gateway.dto;

/**
 * Created by Alice on 1/29/2016.
 */
public class PlaylistAddWrapper {
    private String playlistName;
    private String playlistPrivacy;
    private String token;
    private String userId;

    public String getPlaylistName() {
        return playlistName;
    }

    public void setPlaylistName(String playlistName) {
        this.playlistName = playlistName;
    }

    public String getPlaylistPrivacy() {
        return playlistPrivacy;
    }

    public void setPlaylistPrivacy(String playlistPrivacy) {
        this.playlistPrivacy = playlistPrivacy;
    }

    public String getToken() {
        return token;
    }

    public void setToken(String token) {
        this.token = token;
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }
}
