package muxar.gateway.dto;

public class DeletePlaylistWrapper {
	private String userId;
	private String token;
    private String playlistName;
    private String playlistId;
    private String playlistPrivacy;

	public DeletePlaylistWrapper(){
		
	}
    
	public DeletePlaylistWrapper(String userId, String token, String playlistId, String playlistName, String playlistPrivacy) {
		this.userId = userId;
		this.token = token;
		this.playlistId = playlistId;
		this.playlistName = playlistName;
		this.playlistPrivacy = playlistPrivacy;
	}
	
	public String getUserId() {
		return userId;
	}
	public void setUserId(String userId) {
		this.userId = userId;
	}
	public String getToken() {
		return token;
	}
	public void setToken(String token) {
		this.token = token;
	}
	public String getPlaylistName() {
		return playlistName;
	}

	public void setPlaylistName(String playlistName) {
		this.playlistName = playlistName;
	}

	public String getPlaylistId() {
		return playlistId;
	}

	public void setPlaylistId(String playlistId) {
		this.playlistId = playlistId;
	}

	public String getPlaylistPrivacy() {
		return playlistPrivacy;
	}

	public void setPlaylistPrivacy(String playlistPrivacy) {
		this.playlistPrivacy = playlistPrivacy;
	}
}
