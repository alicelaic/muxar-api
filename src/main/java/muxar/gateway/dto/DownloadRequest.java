package muxar.gateway.dto;

/**
 * Created by Alice on 1/30/2016.
 */
public class DownloadRequest {
    private String userId;
    private String token;
    private String playlistResource;

    public DownloadRequest() {
    }

    public DownloadRequest(String userId, String token, String playlistResource) {
        this.userId = userId;
        this.token = token;
        this.playlistResource = playlistResource;
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public String getToken() {
        return token;
    }

    public void setToken(String token) {
        this.token = token;
    }

    public String getPlaylistResource() {
        return playlistResource;
    }

    public void setPlaylistResource(String playlistResource) {
        this.playlistResource = playlistResource;
    }
}
