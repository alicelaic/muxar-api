package muxar.gateway;

import muxar.gateway.domain.MusicFacebook;

/**
 * Created by Alice on 1/25/2016.
 */
public interface GatewayService {

    /**
     * @param userId unique userId from facebook
     * @param accessToken access token generated to make FB Api call
     *
     */
    MusicFacebook getMusic(String userId, String accessToken);
}
