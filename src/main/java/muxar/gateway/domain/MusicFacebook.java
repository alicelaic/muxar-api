package muxar.gateway.domain;

import java.io.Serializable;
import java.util.List;

/**
 * Created by Alice on 1/25/2016.
 */
public class MusicFacebook implements Serializable{

    private List<MusicItemFacebook> data;
    private Paging paging;

    public List<MusicItemFacebook> getData() {
        return data;
    }

    public void setData(List<MusicItemFacebook> data) {
        this.data = data;
    }

    public Paging getPaging() {
        return paging;
    }

    public void setPaging(Paging paging) {
        this.paging = paging;
    }
}
