package muxar.gateway.impl;

import muxar.gateway.GatewayService;
import muxar.gateway.domain.MusicFacebook;
import muxar.utils.Constants;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import java.text.MessageFormat;

/**
 * Class used to make HTTP/HTTPS requests to external
 * Created by Alice on 1/25/2016.
 */
@Service
public class GatewayServiceImpl implements GatewayService{

    @Autowired
    private RestTemplate rt;

    public MusicFacebook getMusic(String userId, String accessToken) {
        String musicUrlFormatted = MessageFormat.format(Constants.URLS.MUSIC_FB_URL, userId, accessToken);
        ResponseEntity<MusicFacebook> responseEntity = rt.getForEntity(musicUrlFormatted, MusicFacebook.class);

        if(HttpStatus.OK.equals(responseEntity.getStatusCode())) {
            return responseEntity.getBody();
        }

        //return empty as not being any music preferences from user
        return new MusicFacebook();
    }
}
