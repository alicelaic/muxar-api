package muxar.utils;

public class Constants {

	public static final String LAST_SONG_RECOMMENDATION_MODEL = "recommendationByLastSong";
	public static final String ALL_USER_PLAYLISTS_MODEL = "userPlaylists";

	public static final String VIRT_URL = "virtuoso.url";
	public static final String VIRT_USERNAME = "virtuoso.username";
	public static final String VIRT_PASSWORD = "virtuoso.password";
	public static final String VIRT_GRAPH = "virtuoso.graph.name";

	public class URLS{
		public static final String MUSIC_FB_URL = "https://graph.facebook.com/v2.5/{0}/music?access_token={1}";
	}

	public static final String TOKEN = "token";
	public static final String SEARCH_BY_ARTIST_TAG = "artist:";
	public static final String SEARCH_BY_GENRE_TAG = "genre:";
}
