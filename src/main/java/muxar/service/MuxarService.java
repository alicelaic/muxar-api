package muxar.service;

import com.hp.hpl.jena.query.Query;
import com.hp.hpl.jena.query.QuerySolution;
import com.hp.hpl.jena.query.ResultSet;

import muxar.dto.MusicArtist;
import muxar.dto.MusicItem;
import muxar.virtuoso.Graph;
import virtuoso.jena.driver.VirtuosoQueryExecution;
import virtuoso.jena.driver.VirtuosoQueryExecutionFactory;

import java.util.ArrayList;
import java.util.List;

public class MuxarService {

	public static List<MusicItem> getSongsListenedByOtherUsersFor(String userId) {
		List<MusicItem> songs = new ArrayList<MusicItem>();
		Query query = MuxarQueryBuilder.getSongsListenedByOtherUserFor(userId);
		VirtuosoQueryExecution qe = VirtuosoQueryExecutionFactory.create(query,
				new Graph().connection());

		ResultSet result = qe.execSelect();
		while (result.hasNext()) {
			QuerySolution solution = result.next();
			if (solution != null) {

				MusicItem song = new MusicItem();
				MusicArtist artist = new MusicArtist(solution.getLiteral(
						"?artistName").toString());
				artist.setResourceName(solution.getResource("artist")
						.toString());
				artist.setImage(solution.getResource("img").toString());
				song.setArtist(artist);

				song.setName(solution.getLiteral("songName").toString());
				song.setResourceName(solution.getResource("song").toString());
				songs.add(song);
				System.out.println(song.getName());
			}
		}

		return songs;

	}

	public static String getArtistListenedBy(String userId) {
		Query query = MuxarQueryBuilder.getArtistListenedBy(userId);
		VirtuosoQueryExecution qe = VirtuosoQueryExecutionFactory.create(query,
				new Graph().connection());

		ResultSet result = qe.execSelect();
		while (result.hasNext()) {
			QuerySolution solution = result.next();
			if (solution != null) {

				return solution.getLiteral("artistName").toString();
			}
		}

		return "";

	}

	public static List<MusicItem> getSongsListenedBy(String userId) {
		List<MusicItem> songs = new ArrayList<MusicItem>();
		Query query = MuxarQueryBuilder.getSongsListenedBy(userId);
		VirtuosoQueryExecution qe = VirtuosoQueryExecutionFactory.create(query,
				new Graph().connection());

		ResultSet result = qe.execSelect();
		while (result.hasNext()) {
			QuerySolution solution = result.next();
			if (solution != null) {

				MusicItem song = new MusicItem();
				MusicArtist artist = new MusicArtist(solution.getLiteral(
						"?artistName").toString());
				artist.setResourceName(solution.getResource("artist")
						.toString());
				artist.setImage(solution.getResource("img").toString());
				song.setArtist(artist);

				song.setName(solution.getLiteral("songName").toString());
				song.setResourceName(solution.getResource("song").toString());
				songs.add(song);
				System.out.println(song.getName());
			}
		}

		return songs;

	}
	
	public static List<MusicItem> getAllSongsListenedBy(String userId) {
		List<MusicItem> songs = new ArrayList<MusicItem>();
		Query query = MuxarQueryBuilder.getAllSongsListenedBy(userId);
		VirtuosoQueryExecution qe = VirtuosoQueryExecutionFactory.create(query,
				new Graph().connection());

		ResultSet result = qe.execSelect();
		while (result.hasNext()) {
			QuerySolution solution = result.next();
			if (solution != null) {

				MusicItem song = new MusicItem();
				MusicArtist artist = new MusicArtist(solution.getLiteral(
						"?artistName").toString());
				artist.setResourceName(solution.getResource("artist")
						.toString());
				artist.setImage(solution.getResource("img").toString());
				song.setArtist(artist);

				song.setName(solution.getLiteral("songName").toString());
				song.setResourceName(solution.getResource("song").toString());
				songs.add(song);
				System.out.println(song.getName());
			}
		}

		return songs;

	}

}
