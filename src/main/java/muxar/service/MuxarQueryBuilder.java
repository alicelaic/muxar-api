package muxar.service;

import com.hp.hpl.jena.query.ParameterizedSparqlString;
import com.hp.hpl.jena.query.Query;
import com.hp.hpl.jena.sparql.expr.Expr;

import muxar.utils.Vocabulary;

public class MuxarQueryBuilder {

	public static final String GET_SONGS_SELECT = "SELECT ?song ?artist ?songName ?artistName ?img";

	public static Query getSongsListenedByOtherUserFor(String userId) {

		ParameterizedSparqlString qs = new ParameterizedSparqlString();

		qs.setNsPrefix(Vocabulary.RDF_PREFIX, Vocabulary.RDF_URI);
		qs.setNsPrefix(Vocabulary.MUXAR_PREFIX, Vocabulary.MUXAR_URI);
		qs.setNsPrefix(Vocabulary.SCHEMA_PREFIX, Vocabulary.SCHEMA_URI);
		qs.setNsPrefix(Vocabulary.DBPedia.ONTOLOGY_PREFIX,
				Vocabulary.DBPedia.ONTOLOGY_URI);
		qs.setNsPrefix(Vocabulary.FOAF_PREFIX, Vocabulary.FOAF_URI);

		qs.append(GET_SONGS_SELECT);
		qs.append(" WHERE {\n ");
		qs.append("?user mx:listen ?song . \n");
		qs.append("?song foaf:name ?songName. \n");
		qs.append("?song schema:byArtist ?artist. \n");
		qs.append("?artist foaf:name ?artistName. \n ");
		qs.append("?artist schema:image ?img. \n");
		qs.append("FILTER (?user != ");
		qs.append("mx:" + userId);
		qs.append(" )} LIMIT 3");

		return qs.asQuery();

	}

	public static Query getSongsListenedBy(String userId) {

		ParameterizedSparqlString qs = MuxarQueryBuilder
				.songsListenedBy(userId);
		qs.append(" LIMIT 3");

		return qs.asQuery();
	}

	private static ParameterizedSparqlString songsListenedBy(String userId) {
		ParameterizedSparqlString qs = new ParameterizedSparqlString();

		qs.setNsPrefix(Vocabulary.RDF_PREFIX, Vocabulary.RDF_URI);
		qs.setNsPrefix(Vocabulary.MUXAR_PREFIX, Vocabulary.MUXAR_URI);
		qs.setNsPrefix(Vocabulary.SCHEMA_PREFIX, Vocabulary.SCHEMA_URI);
		qs.setNsPrefix(Vocabulary.DBPedia.ONTOLOGY_PREFIX,
				Vocabulary.DBPedia.ONTOLOGY_URI);
		qs.setNsPrefix(Vocabulary.FOAF_PREFIX, Vocabulary.FOAF_URI);

		qs.append(GET_SONGS_SELECT);
		qs.append(" WHERE {\n ");
		qs.append("?user mx:listen ?song . \n");
		qs.append("?song foaf:name ?songName. \n");
		qs.append("?song schema:byArtist ?artist. \n");
		qs.append("?artist foaf:name ?artistName. \n ");
		qs.append("?artist schema:image ?img. \n");
		qs.append("FILTER (?user = ");
		qs.append("mx:" + userId);
		qs.append(" )}");

		return qs;
	}

	public static Query getAllSongsListenedBy(String userId) {

		return MuxarQueryBuilder.songsListenedBy(userId).asQuery();

	}

	public static Query getArtistListenedBy(String userId) {

		ParameterizedSparqlString qs = new ParameterizedSparqlString();

		qs.setNsPrefix(Vocabulary.MUXAR_PREFIX, Vocabulary.MUXAR_URI);
		qs.setNsPrefix(Vocabulary.SCHEMA_PREFIX, Vocabulary.SCHEMA_URI);
		qs.setNsPrefix(Vocabulary.FOAF_PREFIX, Vocabulary.FOAF_URI);

		qs.append("SELECT ?artistName ");
		qs.append(" WHERE {\n ");
		qs.append("?user mx:listen ?song . \n");
		qs.append("?artist foaf:name ?artistName . \n");
		qs.append("?song schema:byArtist ?artist . } LIMIT 50");

		qs.setIri("user", Vocabulary.MUXAR_URI + userId);

		return qs.asQuery();

	}
}
