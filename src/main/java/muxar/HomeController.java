package muxar;

import com.hp.hpl.jena.graph.Node;
import com.hp.hpl.jena.graph.Triple;

import muxar.dto.MusicArtist;
import muxar.dto.MusicItem;
import muxar.dto.User;
import muxar.facebook.service.FacebookService;
import muxar.gateway.domain.MusicFacebook;
import muxar.gateway.domain.MusicItemFacebook;
import muxar.gateway.dto.*;
import muxar.security.SecurityService;
import muxar.service.DbpediaService;
import muxar.service.MuxarService;
import muxar.utils.Constants;
import muxar.utils.Vocabulary;
import muxar.utils.Vocabulary.Property;
import muxar.virtuoso.Graph;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import virtuoso.jena.driver.VirtGraph;

import javax.ws.rs.Consumes;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.ThreadLocalRandom;

@Controller
public class HomeController {

	@Autowired
	private SecurityService securityService;

	@Autowired
	private FacebookService facebookService;

	@RequestMapping(value = "/recommendation", method = RequestMethod.POST)
	@Consumes("application/json")
	public ResponseEntity<MusicRecommendation> getRecommendations(
			@RequestBody User user) {
		// validate token
		if (!securityService.validateToken(user.getFbAccessToken())) {
			return new ResponseEntity<MusicRecommendation>(
					new MusicRecommendation(), HttpStatus.OK);
		}

		List<MusicItem> songsOfArtist = DbpediaService.getSongsOf(MuxarService
				.getArtistListenedBy(user.getUserId()));
		List<MusicItem> songsOfUser = MuxarService.getSongsListenedBy(user
				.getUserId());
		List<MusicItem> songsOfMuxarUsers = MuxarService
				.getSongsListenedByOtherUsersFor(user.getUserId());

		MusicFacebook musicFacebook = user.getMusicFacebook();
		int index = ThreadLocalRandom.current().nextInt(0, musicFacebook.getData().size());
		List<MusicItem> songsFromFB = DbpediaService.getSongsOf(musicFacebook
				.getData().get(index).getName());
		MusicRecommendation recommendation = new MusicRecommendation(
				songsOfArtist, songsOfUser, songsOfMuxarUsers, songsFromFB);

		return new ResponseEntity<MusicRecommendation>(recommendation,
				HttpStatus.OK);
	}

	@RequestMapping(value = "/search", method = RequestMethod.POST)
	public ResponseEntity<MusicWrapper> search(@RequestBody MusicRequest request) {
		if (!securityService.validateToken(request.getToken())) {
			return new ResponseEntity<MusicWrapper>(new MusicWrapper(),
					HttpStatus.BAD_REQUEST);
		}

		List<MusicItem> songs = new ArrayList<MusicItem>();
		String searchText = request.getSearchText();

		if (searchText.startsWith(Constants.SEARCH_BY_ARTIST_TAG)) {
			searchText = searchText.substring(Constants.SEARCH_BY_ARTIST_TAG
					.length());
			songs = DbpediaService.getSongsOfArtist(searchText);
		} else if (searchText.startsWith(Constants.SEARCH_BY_GENRE_TAG)) {
			searchText = searchText.substring(Constants.SEARCH_BY_GENRE_TAG
					.length());
			songs = DbpediaService.getSongsByGenre(searchText);
		} else {
			songs = DbpediaService.getSongsBy(searchText);
		}

		MusicWrapper wrapper = new MusicWrapper();
		wrapper.setMusicList(songs);
		
		return new ResponseEntity<MusicWrapper>(wrapper, HttpStatus.OK);
	}

	@RequestMapping(value = "/play", method = RequestMethod.POST)
	@ResponseBody
	public ResponseEntity<Void> play(@RequestBody PlayWrapper playWrapper) {
		String token = playWrapper.getToken();
		String userId = playWrapper.getUserId();
		String songResource = playWrapper.getSongResource();
		String artistName = playWrapper.getArtistName();
		String artistImage = playWrapper.getArtistImage();
		String artistResource = playWrapper.getArtistResource();
		String songName = playWrapper.getSongName();

		if (!securityService.validateToken(token)) {
			return new ResponseEntity<Void>(HttpStatus.BAD_REQUEST);
		}

		// TODO please move this logic in a service
		Node user = Node.createURI(Vocabulary.MUXAR_URI + userId);
		Node listenProp = Node
				.createURI(Vocabulary.MUXAR_URI + Property.LISTEN);
		Node songNode = Node.createURI(songResource);

		MusicArtist artist = new MusicArtist(artistName);
		artist.setImage(artistImage);
		artist.setResourceName(artistResource);

		MusicItem song = new MusicItem();
		song.setName(songName);
		song.setResourceName(songResource);
		song.setArtist(artist);

		VirtGraph virtGraph = new Graph().connection();
		Triple listen = new Triple(user, listenProp, songNode);

		if (virtGraph.contains(listen)) {
			return new ResponseEntity<Void>(HttpStatus.OK);
		}

		if (!virtGraph.contains(artist.rdfIsArtist())) {

			virtGraph.add(artist.rdfIsArtist());
			virtGraph.add(artist.rdfHasName());
			virtGraph.add(artist.rdfHasImage());
		}

		if (!virtGraph.contains(song.rdfIsMusicRecording())) {

			virtGraph.add(song.rdfIsMusicRecording());
			virtGraph.add(song.rdfHasName());
			virtGraph.add(song.rdfHasArtist());
		}

		virtGraph.add(listen);

		return new ResponseEntity<Void>(HttpStatus.OK);
	}

}
