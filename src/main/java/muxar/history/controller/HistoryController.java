package muxar.history.controller;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import javax.ws.rs.Consumes;
import javax.ws.rs.Produces;

import muxar.dto.MusicItem;
import muxar.gateway.dto.MusicRequest;
import muxar.gateway.dto.MusicWrapper;
import muxar.gateway.dto.PlaylistWrapper;
import muxar.playlist.domain.Playlist;
import muxar.security.SecurityService;
import muxar.service.DbpediaService;
import muxar.service.MuxarService;
import muxar.utils.Constants;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

/**
 * @author alaic
 */
@Controller
public class HistoryController {

	@Autowired
	private SecurityService securityService;

	@RequestMapping(value = "/history", method = RequestMethod.GET)
	@Consumes("application/json")
	@Produces("application/json")
	public ResponseEntity<MusicWrapper> getListenedMusic(
			@RequestParam(value = "auth_token") String token, @RequestParam(value="userId") String userId) {
		if (!securityService.validateToken(token)) {
			return new ResponseEntity<MusicWrapper>(new MusicWrapper(),
					HttpStatus.BAD_REQUEST);
		}
		
		MusicWrapper wrapper = new MusicWrapper();
		wrapper.setMusicList(MuxarService.getAllSongsListenedBy(userId));

		return new ResponseEntity(wrapper, HttpStatus.OK);
	}

}
