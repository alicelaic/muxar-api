package muxar.history.service.impl;

import muxar.history.service.HistoryService;
import org.springframework.stereotype.Service;

/**
 * Class to be used to implement functionalities for history
 *
 * Created by Alice on 1/28/2016.
 */
@Service
public class HistoryServiceImpl implements HistoryService{
}
