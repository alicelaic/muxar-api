package muxar.security.interceptor;

import muxar.security.SecurityService;
import muxar.utils.Constants;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.servlet.handler.HandlerInterceptorAdapter;

import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * Interceptor used to validate token for every request
 * Created by Alice on 1/26/2016.
 */
public class SecurityInterceptor extends HandlerInterceptorAdapter {
    @Autowired
    private SecurityService securityService;

    @Override
    public boolean preHandle(HttpServletRequest request, HttpServletResponse response, Object handler) throws Exception {
        String tokenValue = getToken(request.getCookies());
        System.out.println("Token: " + tokenValue);
        return securityService.validateToken(tokenValue);
    }

    private String getToken(Cookie[] cookies) {
        for(Cookie cookie: cookies) {
            if(Constants.TOKEN.equalsIgnoreCase(cookie.getName())){
                return cookie.getValue();
            }
        }

        return StringUtils.EMPTY;
    }
}
