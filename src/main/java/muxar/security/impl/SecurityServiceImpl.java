package muxar.security.impl;

import muxar.security.SecurityService;

import com.hp.hpl.jena.graph.Node;
import com.hp.hpl.jena.graph.Triple;
import com.hp.hpl.jena.query.ParameterizedSparqlString;
import com.hp.hpl.jena.query.QuerySolution;
import com.hp.hpl.jena.query.ResultSet;
import muxar.ApplicationInit;
import muxar.utils.Constants;
import muxar.utils.Vocabulary;
import org.springframework.stereotype.Service;

import virtuoso.jena.driver.VirtGraph;
import virtuoso.jena.driver.VirtuosoQueryExecution;
import virtuoso.jena.driver.VirtuosoQueryExecutionFactory;

/**
 * Created by Alice on 1/25/2016.
 */
@Service
public class SecurityServiceImpl implements SecurityService {

	private VirtGraph graph = null;

	public void init() {
		if (graph != null) {
			return;
		}
		String url = ApplicationInit.getProperty(Constants.VIRT_URL);
		String usernameVirt = ApplicationInit
				.getProperty(Constants.VIRT_USERNAME);
		String passwordVirt = ApplicationInit
				.getProperty(Constants.VIRT_PASSWORD);
		graph = new VirtGraph("MUXAR", url, usernameVirt, passwordVirt);
	}

	public void persistToken(String userId, String token) {
		init();
		Node s = Node.createURI(Vocabulary.MUXAR_URI + userId);
		Node p = Node.createURI(Vocabulary.MUXAR_HAS_TOKEN);
		Node o = Node.createURI(Vocabulary.MUXAR_URI + token);

		graph.add(new Triple(s, p, o));
	}

	public boolean validateToken(String token) {
		init();
		ParameterizedSparqlString qs = new ParameterizedSparqlString();
		qs.setNsPrefix(Vocabulary.MUXAR_PREFIX, Vocabulary.MUXAR_URI);
		StringBuilder sparql = new StringBuilder();
		sparql.append("SELECT ?user WHERE { ?user <");
		sparql.append(Vocabulary.MUXAR_HAS_TOKEN);
		sparql.append("> <" + Vocabulary.MUXAR_URI).append(token).append("> }");

		qs.append(sparql);

		VirtuosoQueryExecution qe = VirtuosoQueryExecutionFactory.create(
				qs.asQuery(), graph);
		ResultSet result = qe.execSelect();
		while (result.hasNext()) {
			QuerySolution solution = result.next();
			if (solution != null) {
				return true;
			}
		}
		return false;
	}
}
