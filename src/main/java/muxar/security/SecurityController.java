package muxar.security;

import muxar.gateway.dto.TokenWrapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import javax.ws.rs.Consumes;
import javax.ws.rs.Produces;

/**
 * Created by Alice on 1/29/2016.
 */
@Controller
public class SecurityController {
    @Autowired
    private SecurityService securityService;

    @RequestMapping(value="persistToken",method = RequestMethod.POST)
    @Consumes("application/json")
    public ResponseEntity<Void> persistToken(@RequestBody TokenWrapper tokenWrapper){
        securityService.persistToken(tokenWrapper.getUserId(), tokenWrapper.getToken());
        return new ResponseEntity<Void>(HttpStatus.OK);
    }

    @RequestMapping(value="validateToken",method = RequestMethod.POST)
    @Consumes("application/json")
    @Produces("application/json")
    public ResponseEntity<Boolean> validateToken(@RequestBody String token){
        Boolean validate  = securityService.validateToken(token);
        return new ResponseEntity<Boolean>(validate, HttpStatus.OK);
    }
}
