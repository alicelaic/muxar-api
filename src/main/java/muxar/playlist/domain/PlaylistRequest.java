package muxar.playlist.domain;

import com.fasterxml.jackson.annotation.JsonProperty;

/**
 * Object used to get request
 * Created by Alice on 1/28/2016.
 */
public class PlaylistRequest {

    @JsonProperty("auth_token")
    private String token;
    private String songId;
    private String playlistId;
    private String songName;
    private String artistName;
    private String artistResource;

    public PlaylistRequest() {
    }

    public PlaylistRequest(String token, String songId, String songName, String artistName, String artistResource, String playlistId) {
        this.token = token;
        this.songId = songId;
        this.playlistId = playlistId;
        this.songName = songName;
        this.artistName = artistName;
        this.artistResource = artistResource;
    }

    public String getSongName() {
		return songName;
	}

	public void setSongName(String songName) {
		this.songName = songName;
	}

	public String getArtistName() {
		return artistName;
	}

	public void setArtistName(String artistName) {
		this.artistName = artistName;
	}

	public String getArtistResource() {
		return artistResource;
	}

	public void setArtistResource(String artistResource) {
		this.artistResource = artistResource;
	}

	public String getToken() {
        return token;
    }

    public void setToken(String token) {
        this.token = token;
    }

    public String getSongId() {
        return songId;
    }

    public void setSongId(String songId) {
        this.songId = songId;
    }

    public String getPlaylistId() {
        return playlistId;
    }

    public void setPlaylistId(String playlistId) {
        this.playlistId = playlistId;
    }
}
