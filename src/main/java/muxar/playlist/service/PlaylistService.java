package muxar.playlist.service;

import muxar.dto.MusicItem;
import muxar.dto.User;
import muxar.gateway.dto.DownloadRequest;
import muxar.playlist.domain.Playlist;

import java.util.List;

/**
 * Created by Alice on 1/3/2016.
 */
public interface PlaylistService {
    void addSongToPlaylist(String playlistId, String songId, String songName, String artistName, String artistResource);
    List<Playlist> getPlaylistsForUser(String userId);
    List<Playlist> getPublicPlaylists();
    void addPlaylistForUser(String userId, Playlist playlist);
    void deletePlaylistForUser(String userId, Playlist playlist);
	List<MusicItem> getSongsForPlaylist(String playlistId);
    void downloadRequest(DownloadRequest request);
}
