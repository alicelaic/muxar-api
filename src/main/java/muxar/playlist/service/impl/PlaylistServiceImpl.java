package muxar.playlist.service.impl;

import com.hp.hpl.jena.graph.Node;
import com.hp.hpl.jena.graph.Triple;
import com.hp.hpl.jena.query.ParameterizedSparqlString;
import com.hp.hpl.jena.query.QuerySolution;
import com.hp.hpl.jena.query.ResultSet;

import muxar.dto.MusicArtist;
import muxar.dto.MusicItem;
import muxar.dto.User;
import muxar.gateway.dto.DownloadRequest;
import muxar.playlist.domain.Playlist;
import muxar.playlist.service.PlaylistService;
import muxar.utils.Vocabulary;
import muxar.virtuoso.Graph;
import org.springframework.stereotype.Service;
import virtuoso.jena.driver.VirtGraph;
import virtuoso.jena.driver.VirtuosoQueryExecution;
import virtuoso.jena.driver.VirtuosoQueryExecutionFactory;

import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

/**
 * Created by Alice on 1/3/2016.
 */
@Service
public class PlaylistServiceImpl implements PlaylistService{

    private VirtGraph graph = null;
    public void init() {
        if(graph != null) {
            return;
        }
       graph = new Graph().connection();
    }

    public void addSongToPlaylist(String playlistResource, String songResource, String songName, String artistName,
			String artistResource) {
        init();
        //add song resource as Music Recording
        /*Node s = Node.createURI(songResource);
        Node p = Node.createURI(Vocabulary.RDF_URI + "type");
        Node o = Node.createURI(Vocabulary.SCHEMA_URI + "MusicRecording");

        Triple t = new Triple(s,p,o);
        if(!graph.contains(t)){
            graph.add(t);
        }*/

		MusicArtist artist = new MusicArtist(artistName);
		artist.setResourceName(artistResource);
		if (!graph.contains(artist.rdfIsArtist())) {

			graph.add(artist.rdfIsArtist());
			graph.add(artist.rdfHasName());
		}
		
		MusicItem song = new MusicItem();
		song.setName(songName);
		song.setResourceName(songResource);
		song.setArtist(artist);
		
		if (!graph.contains(song.rdfIsMusicRecording())) {

			graph.add(song.rdfIsMusicRecording());
			graph.add(song.rdfHasName());
			graph.add(song.rdfHasArtist());
		}
        
        //check if playlist exists
        Node s0 = Node.createURI(playlistResource);
        Node p0 = Node.createURI(Vocabulary.RDF_URI + "type");
        Node o0 = Node.createURI(Vocabulary.SCHEMA_URI + "MusicPlaylist");
        Triple t0 = new Triple(s0, p0, o0);
        if(!graph.contains(t0)) {
            graph.add(t0);
        }

        //assuming that song and playlist already exist in graph
        Node s1 = Node.createURI(songResource);
        Node p1 = Node.createURI(Vocabulary.SCHEMA_URI + Vocabulary.Property.SCHEMA_IN_PLAYLIST);
        Node o1 = Node.createURI(playlistResource);

        Triple t1 = new Triple(s1,p1,o1);
        if(!graph.contains(t1)){
            graph.add(t1);
        }
    }

    /**
     * returns the playlists for an user
     * @param userId specific user logged in
     */
    public List<Playlist> getPlaylistsForUser(String userId) {
        init();
        List<Playlist> playlists = new ArrayList<Playlist>();
        ParameterizedSparqlString qs = new ParameterizedSparqlString();
        qs.setNsPrefix(Vocabulary.MUXAR_PREFIX,
                Vocabulary.MUXAR_URI);
        qs.setNsPrefix(Vocabulary.SCHEMA_PREFIX,
                Vocabulary.SCHEMA_URI);
        qs.setNsPrefix(Vocabulary.FOAF_PREFIX,
                Vocabulary.FOAF_URI);
        String sparql = "SELECT DISTINCT ?playlist ?name ?privacy \n" +
                "WHERE {{ mx:" + userId +" mx:hasPlaylist ?playlist.\n " +
                "?playlist mx:childOf ?playlist2. \n" +
                "?playlist2 foaf:name ?name.\n" +
                "} UNION" +
                "{ mx:" + userId + " mx:hasPlaylist ?playlist.\n" +
                "?playlist foaf:name ?name. \n" +
                "?playlist schema:accessibilityControl ?privacy.\n"+
                "}}"
                ;
        qs.append(sparql);

        VirtuosoQueryExecution qe = VirtuosoQueryExecutionFactory.create(qs.asQuery(), graph);
        ResultSet result = qe.execSelect();
        while (result.hasNext()) {
            QuerySolution solution = result.next();
            if(solution != null) {
                //System.out.println("playlist: " + solution.get("playlist").toString());
                //System.out.println("name: " + solution.get("name").toString());
                //System.out.println("privacy: " + solution.get("privacy").toString());
                Playlist p = new Playlist(solution.get("name").toString(), solution.get("playlist").toString());
                p.setShared(solution.get("privacy") == null || ("shared".equalsIgnoreCase(solution.get("privacy").toString())));
                playlists.add(p);
            }

        }
        return playlists;
    }


    /**
     *Return public playlists
    */

    public List<Playlist> getPublicPlaylists() {
        init();
        List<Playlist> playlists = new ArrayList<Playlist>();
        ParameterizedSparqlString qs = new ParameterizedSparqlString();
        qs.setNsPrefix(Vocabulary.MUXAR_PREFIX,
                Vocabulary.MUXAR_URI);
        qs.setNsPrefix(Vocabulary.SCHEMA_PREFIX,
                Vocabulary.SCHEMA_URI);
        qs.setNsPrefix(Vocabulary.FOAF_PREFIX,
                Vocabulary.FOAF_URI);
        String sparql = "SELECT * " +
                "WHERE {?user mx:hasPlaylist ?playlist . " +
                "?playlist foaf:name ?name. " +
                "?playlist schema:accessibilityControl ?privacy}";
        qs.append(sparql);

        qs.setLiteral("privacy", "shared");
        VirtuosoQueryExecution qe = VirtuosoQueryExecutionFactory.create(qs.asQuery(), graph);

        ResultSet result = qe.execSelect();
        while (result.hasNext()) {
            QuerySolution solution = result.next();
            if(solution != null) {
                //System.out.println("playlist: " + solution.get("playlist").toString());
                //System.out.println("name: " + solution.get("name").toString());
                //System.out.println("privacy: " + solution.get("privacy").toString());
                Playlist p = new Playlist(solution.get("name").toString(), solution.get("playlist").toString());
                p.setShared(true);
                playlists.add(p);
            }

        }
        return playlists;
    }

    public void addPlaylistForUser(String userId, Playlist playlist) {
        init();

        System.out.println("start adding new playlist");

        String playlistResource = Vocabulary.MUXAR_URI + playlist.getId();

        //add playlist resource as Music Playlist
        Triple rdfPlaylist = playlist.rdfIsPlaylist();
        if(!graph.contains(rdfPlaylist)){
            graph.add(rdfPlaylist);
        }

        //playlist foaf:name name
        Triple rdfPlaylistName = playlist.rdfPlaylistHasName();
        if(!graph.contains(rdfPlaylistName)){
            graph.add(rdfPlaylistName);
        }

        // playlist schema:accessibilityControl privacy
        Triple rdfPlaylistPrivacy = playlist.rdfPlaylistHasPrivacy();
        if(!graph.contains(rdfPlaylistPrivacy)){
            graph.add(rdfPlaylistPrivacy);
        }

        //assuming that playlist already exist in graph => user has playlist
        Node s = Node.createURI(Vocabulary.MUXAR_URI + userId);
        Node p = Node.createURI(Vocabulary.MUXAR_URI + Vocabulary.Property.SCHEMA_HAS_PLAYLIST);
        Node o = Node.createURI(playlistResource);

        Triple rdfUserHasPlaylist = new Triple(s, p, o);
        if(!graph.contains(rdfUserHasPlaylist)){
            graph.add(rdfUserHasPlaylist);
        }

        /*System.out.println(Vocabulary.MUXAR_URI + user.getUserId() + " ----- " + Vocabulary.MUXAR_URI
        		+ Vocabulary.Property.SCHEMA_HAS_PLAYLIST + " ---- " + playlistResource);*/

        System.out.println("done adding playlist to user");
        
    }
    

	public List<String> getSongResourcesOfPlaylists(String playlistID) {
        init();
        List<String> songResources = new ArrayList<String>();
        ParameterizedSparqlString qs = new ParameterizedSparqlString();
        qs.setNsPrefix(Vocabulary.MUXAR_PREFIX,
                Vocabulary.MUXAR_URI);
        qs.setNsPrefix(Vocabulary.SCHEMA_PREFIX,
                Vocabulary.SCHEMA_URI);
        qs.setNsPrefix(Vocabulary.FOAF_PREFIX,
                Vocabulary.FOAF_URI);
        qs.setNsPrefix(Vocabulary.RDF_PREFIX,
                Vocabulary.RDF_URI);
        
        String sparql = "SELECT * \n"
                + "WHERE { \n"
                + "?songResource schema:inPlaylist mx:" + playlistID + " .\n"
                + "?songResource rdf:type schema:MusicRecording \n"
                + "}";
        System.out.println(sparql);
        qs.append(sparql);

        VirtuosoQueryExecution qe = VirtuosoQueryExecutionFactory.create(qs.asQuery(), graph);
        ResultSet result = qe.execSelect();
        while (result.hasNext()) {
            QuerySolution solution = result.next();
            if(solution != null) {
                System.out.println("song in playlist: " + solution.getResource("songResource").toString());
                songResources.add(solution.get("songResource").toString());
            }

        }
        return songResources;
    }

	@SuppressWarnings("deprecation")
	public void deletePlaylistForUser(String userId, Playlist playlist) {
        init();

        System.out.println("start delete playlist");
        String playlistResource = Vocabulary.MUXAR_URI + playlist.getId();
       
        try {
	        //playlist foaf:name name
	        Triple rdfPlaylistName = playlist.rdfPlaylistHasName();
	        if(graph.contains(rdfPlaylistName)){
	            graph.performDelete(rdfPlaylistName);
	        }
        } catch (Exception ex) {
        	System.out.println("Exception thrown @performDelete(rdfPlaylistName): " + ex.toString());
        }

        try {
	        // playlist schema:accessibilityControl privacy
	        Triple rdfPlaylistPrivacy = playlist.rdfPlaylistHasPrivacy();
	        if(graph.contains(rdfPlaylistPrivacy)){
	            graph.performDelete(rdfPlaylistPrivacy);
	        }
		} catch (Exception ex) {
			System.out.println("Exception thrown @performDelete(rdfPlaylistPrivacy): " + ex.toString());
		}
        
        List<String> songResources = getSongResourcesOfPlaylists(playlist.getId());
        
        for(String res : songResources) {
        	try {
    	        //song schema:inPlaylist playlist
        		Node s = Node.createURI(res);
                Node p = Node.createURI(Vocabulary.SCHEMA_URI + "inPlaylist");
                Node o = Node.createURI(Vocabulary.MUXAR_URI + playlistResource);
                
    	        Triple rdfPlaylistSong = new Triple(s, p, o);
    	        if(graph.contains(rdfPlaylistSong)){
    	            graph.performDelete(rdfPlaylistSong);
    	        }
            } catch (Exception ex) {
            	System.out.println("Exception thrown @performDelete(rdfPlaylistSong): " + ex.toString());
            }
        }
        
        try {
	        //assuming that playlist components are already deleted in graph => delete user has playlist
	        Node s = Node.createURI(Vocabulary.MUXAR_URI + userId);
	        Node p = Node.createURI(Vocabulary.MUXAR_URI + Vocabulary.Property.SCHEMA_HAS_PLAYLIST);
	        Node o = Node.createURI(playlistResource);
	
	        Triple rdfUserHasPlaylist = new Triple(s, p, o);
	        if(graph.contains(rdfUserHasPlaylist)){
	            graph.performDelete(rdfUserHasPlaylist);
	        }
			
	       //assuming that playlist components are already deleted in graph => delete playlist resource
	        Triple rdfPlaylistResource = playlist.rdfIsPlaylist();
	        if(graph.contains(rdfPlaylistResource)){
	            graph.performDelete(rdfPlaylistResource);
	        }
        } catch (Exception ex) {
        	System.out.println("Exception thrown @performDelete(rdfPlaylistPrivacy): " + ex.toString());
        }
        
        /*System.out.println(Vocabulary.MUXAR_URI + user.getUserId() + " ----- " + Vocabulary.MUXAR_URI
        		+ Vocabulary.Property.SCHEMA_HAS_PLAYLIST + " ---- " + playlistResource);*/
        System.out.println("done delete playlist from user");
        
    }

	@Override
	public List<MusicItem> getSongsForPlaylist(String playlistId) {
		
		System.out.println("playlistService-api get songs for playlist");
		
		init();
        List<MusicItem> songs = new ArrayList<MusicItem>();
        ParameterizedSparqlString qs = new ParameterizedSparqlString();
        qs.setNsPrefix(Vocabulary.MUXAR_PREFIX,
                Vocabulary.MUXAR_URI);
        qs.setNsPrefix(Vocabulary.SCHEMA_PREFIX,
                Vocabulary.SCHEMA_URI);
        qs.setNsPrefix(Vocabulary.FOAF_PREFIX,
                Vocabulary.FOAF_URI);
        qs.setNsPrefix(Vocabulary.RDF_PREFIX,
                Vocabulary.RDF_URI);
        
        String sparql = "SELECT ?songResource ?artist ?songName ?artistName\n"
                + "WHERE {{ \n"
                + "?songResource schema:inPlaylist <" + playlistId + "> .\n"
                + "?songResource foaf:name ?songName. \n"
                + "?songResource schema:byArtist ?artist. \n"
                + "?artist foaf:name ?artistName. \n "
                + "}union{\n"
                + "<" + playlistId + "> mx:childOf ?playlist.\n"
                + "?songResource schema:inPlaylist ?playlist.\n"
                + "?songResource foaf:name ?songName. \n"
                + "?songResource schema:byArtist ?artist. \n"
                + "?artist foaf:name ?artistName. \n "
                + "}}";
        System.out.println(sparql);
		qs.append(sparql);
		VirtuosoQueryExecution qe = VirtuosoQueryExecutionFactory.create(qs.asQuery(), graph);
		ResultSet result = qe.execSelect();
		while (result.hasNext()) {
			QuerySolution solution = result.next();
			if (solution != null) {

				MusicItem song = new MusicItem();
				MusicArtist artist = new MusicArtist(solution.getLiteral("?artistName").toString());
				artist.setResourceName(solution.getResource("artist").toString());
				song.setArtist(artist);

				song.setName(solution.getLiteral("songName").toString());
				song.setResourceName(solution.getResource("songResource").toString());
				songs.add(song);
				System.out.println(song.getName());
			}
		}
		
		return songs;
	}
    public void downloadRequest(DownloadRequest request) {
        Triple t, t1, t2;

        String unique =  UUID.randomUUID().toString().replace("-", "");
        Node s = Node.createURI(Vocabulary.MUXAR_URI + request.getUserId());
        Node p = Node.createURI(Vocabulary.MUXAR_URI + Vocabulary.Property.SCHEMA_HAS_PLAYLIST);
        Node o = Node.createURI(Vocabulary.MUXAR_URI + unique);
        t = new Triple(s, p, o);


        Node s1 = Node.createURI(Vocabulary.MUXAR_URI + unique);
        Node p1 = Node.createURI(Vocabulary.MUXAR_URI + Vocabulary.Property.MUXAR_CHILD_OF);
        Node o1 = Node.createURI(request.getPlaylistResource());

        t1 = new Triple(s1, p1, o1);

        Node s2 = Node.createURI(Vocabulary.MUXAR_URI + unique);
        Node p2 = Node.createURI(Vocabulary.MUXAR_URI + Vocabulary.Property.SCHEMA_ACCESIBILITY);
        Node o2 = Node.createLiteral("shared");

        t2 = new Triple(s2, p2, o2);

        graph.add(t);
        graph.add(t1);
        graph.add(t2);
    }
}
