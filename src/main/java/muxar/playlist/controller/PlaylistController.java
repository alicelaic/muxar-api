package muxar.playlist.controller;

import muxar.dto.MusicItem;
import muxar.gateway.dto.DeletePlaylistWrapper;
import muxar.gateway.dto.MusicWrapper;
import muxar.gateway.dto.DownloadRequest;
import muxar.gateway.dto.PlaylistAddWrapper;
import muxar.gateway.dto.PlaylistWrapper;
import muxar.playlist.domain.Playlist;
import muxar.playlist.domain.PlaylistRequest;
import muxar.playlist.service.PlaylistService;
import muxar.security.SecurityService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import javax.ws.rs.Consumes;
import javax.ws.rs.Produces;
import java.util.Collections;
import java.util.List;
import java.util.UUID;

/**
 * Created by Alice on 1/3/2016.
 */
@Controller
public class PlaylistController {

    /**
     * using this, in playlistService will inject your implementation (e.g. PlaylistServiceImpl)
     */
    @Autowired
    private PlaylistService playlistService;

    @Autowired
    private SecurityService securityService;
	/**
	 * This method will get all playlists of a user by his unique ID
	 * @param token
	 * @param userId
	 * @return
	 */
    @RequestMapping(value = "/playlist", method = RequestMethod.GET)
    @Consumes("application/json")
    @Produces("application/json")
    public ResponseEntity<List<Playlist>> getPlaylists(@RequestParam(value="auth_token") String token, @RequestParam(value="userId") String userId) {
        if(!securityService.validateToken(token)){
            return new ResponseEntity<List<Playlist>>(Collections.EMPTY_LIST, HttpStatus.BAD_REQUEST);
        }
        List<Playlist> playlists = playlistService.getPlaylistsForUser(userId);
        PlaylistWrapper returned = new PlaylistWrapper();
        returned.setPlaylistList(playlists);
        return new ResponseEntity(returned, HttpStatus.OK);
    }

    /**
     * Return all public playlists
     * need authentication (auth_token)
     */
    @RequestMapping(value = "playlist/public", method = RequestMethod.GET)
    @Consumes("application/json")
    @Produces("application/json")
    public ResponseEntity<List<Playlist>> getPublicPlaylists(@RequestParam(value="auth_token") String token) {
        if(!securityService.validateToken(token)){
            return new ResponseEntity<List<Playlist>>(Collections.EMPTY_LIST, HttpStatus.BAD_REQUEST);
        }

        List<Playlist> playlists = playlistService.getPublicPlaylists();
        PlaylistWrapper returned = new PlaylistWrapper();
        returned.setPlaylistList(playlists);
        return new ResponseEntity(returned, HttpStatus.OK);
    }

    @RequestMapping(value="playlist/song", method = RequestMethod.POST)
    @Consumes("application/json")
    public ResponseEntity<Void> addSongToPlaylist(@RequestBody PlaylistRequest request) {
        System.out.print("in song");
        if(!securityService.validateToken(request.getToken())){
            return new ResponseEntity<Void>( HttpStatus.BAD_REQUEST);
        }

        playlistService.addSongToPlaylist(request.getPlaylistId(), request.getSongId(), request.getSongName(),
        		request.getArtistName(), request.getArtistResource());
        return new ResponseEntity<Void>(HttpStatus.OK);
    }

    @RequestMapping(value="playlist/download", method = RequestMethod.POST)
    @Consumes("application/json")
    public ResponseEntity<Void> downloadPlaylist(@RequestBody DownloadRequest request) {
        System.out.print("in song");
        if(!securityService.validateToken(request.getToken())){
            return new ResponseEntity<Void>( HttpStatus.BAD_REQUEST);
        }

        playlistService.downloadRequest(request);
        return new ResponseEntity<Void>(HttpStatus.OK);
    }


    @RequestMapping(value = "playlist/add", method = RequestMethod.POST)
    @Consumes("application/json")
    public ResponseEntity<Void> addPlaylist(@RequestBody PlaylistAddWrapper playlistAddWrapper) {
    	
        System.out.println("PlaylistController in api: @addPlaylist");
        
        if(!securityService.validateToken(playlistAddWrapper.getToken())){
            return new ResponseEntity<Void>( HttpStatus.BAD_REQUEST);
        }
        
        String playlistName = playlistAddWrapper.getPlaylistName();
        String playlistPrivacy = playlistAddWrapper.getPlaylistPrivacy();
        String token = playlistAddWrapper.getToken();
        String userId = playlistAddWrapper.getUserId();

        Playlist playlist = new Playlist();
        String uniqueID = UUID.randomUUID().toString().replace("-", "");

        playlist.setId(uniqueID);
        playlist.setName(playlistName);
        playlist.setShared(playlistPrivacy.equalsIgnoreCase("true"));

    	System.out.println("new: " + userId + "  " + playlistName + "   "
    			+ playlistPrivacy+"="+playlist.isShared());

        playlistService.addPlaylistForUser(userId, playlist);

        return new ResponseEntity<Void>(HttpStatus.OK);
    }
    
    
    @RequestMapping(value = "playlist/delete", method = RequestMethod.POST)
    @Consumes("application/json")
    public ResponseEntity<Void> deletePlaylist(@RequestBody DeletePlaylistWrapper wrapper) {
    	
        System.out.println("[DELETE]PlaylistController in api: @addPlaylist");
        
        if(!securityService.validateToken(wrapper.getToken())){
            return new ResponseEntity<Void>( HttpStatus.BAD_REQUEST);
        }
        
        Playlist playlist = new Playlist();
        playlist.setId(wrapper.getPlaylistId());
        playlist.setName(wrapper.getPlaylistName());
        playlist.setShared(wrapper.getPlaylistPrivacy().equalsIgnoreCase("true"));
        
    	/*System.out.println("new: " + SessionUtils.getUser(token).getUserId() + "  " + playlistName + "   "
    			+ playlistPrivacy+"="+playlist.isPrivat());*/

        playlistService.deletePlaylistForUser(wrapper.getUserId(), playlist);

        return new ResponseEntity<Void>(HttpStatus.OK);
    }
    
    /**
     * this method gets all songs from an user playlist
     * @param token
     * @param userId
     * @return
     */
    @RequestMapping(value = "/playlist/song", method = RequestMethod.GET)
    @Consumes("application/json")
    @Produces("application/json")
    public ResponseEntity<List<Playlist>> getSongsForPlaylist(@RequestParam(value="auth_token") String token,
    		@RequestParam(value="playlistId") String playlistId) {
    	System.out.println("playlistController-api get songs for playlist");
        if(!securityService.validateToken(token)){
            return new ResponseEntity<List<Playlist>>(Collections.EMPTY_LIST, HttpStatus.BAD_REQUEST);
        }
        List<MusicItem> songs = playlistService.getSongsForPlaylist(playlistId);
        MusicWrapper returned = new MusicWrapper();
        returned.setMusicList(songs);
        return new ResponseEntity(returned, HttpStatus.OK);
    }
}
