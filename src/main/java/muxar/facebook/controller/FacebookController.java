package muxar.facebook.controller;

import muxar.facebook.service.FacebookService;
import muxar.gateway.domain.MusicFacebook;
import muxar.gateway.dto.RegisterWrapper;
import muxar.security.SecurityService;
import muxar.service.DbpediaService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

/**
 * Created by Alice on 1/24/2016.
 */
@Controller
public class FacebookController {

	@Autowired
	private FacebookService facebookService;

	@Autowired
	private SecurityService securityService;

	/**
	 * Method user to register first time user when logging with facebook in app
	 */
	@RequestMapping(value = "/register", method = RequestMethod.POST)
	public ResponseEntity<Void> createFacebookRelationship(
			@RequestBody RegisterWrapper register) {
		String username = register.getUsername();
		String userId = register.getUserId();
		String token = register.getToken();
		DbpediaService.persistUser(username, userId);
		securityService.persistToken(userId, token);
		return new ResponseEntity<Void>(HttpStatus.OK);
	}

	@RequestMapping(value = "/music", method = RequestMethod.GET)
	public ResponseEntity<MusicFacebook> getMusic(
			@RequestParam(value = "token", defaultValue = "") String token,
			@RequestParam(value = "userId", defaultValue = "") String userId) {
		if (!securityService.validateToken(token)) {
			return new ResponseEntity<MusicFacebook>(new MusicFacebook(),
					HttpStatus.BAD_REQUEST);
		}

		return new ResponseEntity<MusicFacebook>(facebookService.getMusicItems(
				userId, token), HttpStatus.OK);
	}

	// TODO remove session at logout from rdf
	@RequestMapping(value = "/logout", method = RequestMethod.POST)
	public ResponseEntity<Void> logout(
			@RequestParam(value = "token", defaultValue = "") String token) {
		if (!securityService.validateToken(token)) {
			return new ResponseEntity<Void>(HttpStatus.BAD_REQUEST);
		}

		// TODO make a method in a service (eventually SecurityServiceImpl) for
		// logout and removes token for user
		return new ResponseEntity<Void>(HttpStatus.OK);
	}
}
