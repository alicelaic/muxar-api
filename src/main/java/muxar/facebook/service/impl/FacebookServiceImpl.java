package muxar.facebook.service.impl;

import muxar.facebook.service.FacebookService;
import muxar.gateway.GatewayService;
import muxar.gateway.domain.MusicFacebook;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * Class intented to be used to make calls to FB API
 * Created by Alice on 1/25/2016.
 */
@Service
public class FacebookServiceImpl implements FacebookService{

    @Autowired
    private GatewayService gatewayService;

    public MusicFacebook getMusicItems(String userId, String fbToken) {
        return gatewayService.getMusic(userId, fbToken);
    }
}
