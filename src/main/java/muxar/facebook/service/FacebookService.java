package muxar.facebook.service;

import muxar.gateway.domain.MusicFacebook;

/**
 * Created by Alice on 1/25/2016.
 */
public interface FacebookService {
    MusicFacebook getMusicItems(String userId, String token);

}
